const db = require('./../configs/sequelize');
const {DataTypes, Model } = db.Sequelize;

const User = require('./../user/model');
const sequelize = db.sequelize;

class Post extends Model {}

Post.init({
  // Model attributes are defined here
  content: {
    type: DataTypes.STRING
  }
}, {
  // Other model options go here
  sequelize, // We need to pass the connection instance
  modelName: 'posts' // We need to choose the model name
});

Post.User = Post.belongsTo(User);
User.hasMany(Post);

module.exports = Post;