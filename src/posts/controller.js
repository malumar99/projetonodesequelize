const db = require('./../configs/sequelize');
const Post = require('./model');
const User = require('./../user/model');

const {Op} = db.Sequelize;

exports.create = (req,res) =>{
    Post.create({
        content: req.body.content,
        userId: req.body.userId
        // user: {
        //     firstname: req.body.firstname,
        //     lastname: req.body.lastname
        // }
    }, 
    {include:[{
            association:Post.User
        }]

    }).then((post) => {
        res.send(post);
    }).catch((err) => {
        console.log("Erro: "+err);
    });
}


exports.findAll = (req,res) =>{
    Post.findAll({include:User, where :{content : {[Op.iLike]: '%' + req.query.content + '%'}},order:['createdAt']}).then((posts) => {
        res.send(posts);
    });
}

exports.update = (req,res) =>{
    Post.update(
        {
            content:req.body.content
        },
        {
            where: {
                id:req.body.id
            }
        }
    ).then(() => {
        res.send({'message':'ok'});
    }).catch((err) => {
        console.log("Erro: "+err);
    });
}

exports.remove = (req,res) =>{
    Post.destroy({
            where: {
                id:req.body.id
            }
        }
    ).then((affectedRows) => {
        res.send({'message':'ok', 'affectedRows':affectedRows});
    }).catch((err) => {
        console.log("Erro: "+err);
    });
}