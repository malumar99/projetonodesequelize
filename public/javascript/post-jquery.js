Posts = {

    add : () => {

        var t = {};

        t.content = $("#content").val(); 
        t.firstname = $("#firstname").val();
        t.lastname = $("#lastname").val();
        
        $.ajax({
            type: 'POST',
            url: '/post',
            data : t, 
            dataType: "json",
            success: Posts.template
        });
        return false;
    },


    template : (data) => {

        
        var comment = $('<div></div>').attr('id', 'comment-' + data.id).attr('class', 'comment');
        
        var content = $('<textarea></textarea>').attr('class', 'content').attr('disabled', true).html(data.content);
        
        // var user = $('<p></p>').attr('class', 'user').html( 'Por :'+ data.user.firstname + " " + data.user.lastname);
        
        var user = $('<p></p>').attr('class', 'user');

        if(data.user){
            user.html( 'Por :'+ data.user.firstname + " " + data.user.lastname);
        }else{
            user.html( 'Por :'+ $('#users option:selected').text());
        }
        
        var dtCreation = new Date(data.createdAt);
        
        dtCreation = (dtCreation.getDate() < 10 ? "0" + dtCreation.getDate(): dtCreation.getDate()) + 
        "/"+ ((dtCreation.getMonth() + 1) < 10 ? "0" + (dtCreation.getMonth() + 1) : (dtCreation.getMonth() + 1)) + 
        "/" + dtCreation.getFullYear();
        
        var date = $("<span></span>").attr('class', 'date').html(dtCreation);
        
        var btnEdit = $('<button></button>').attr('class', 'edit').html("Editar"); 
        var btnSave = $('<button></button>').attr('class', 'save hidden').html("Salvar");
        var btnRemove = $('<button></button>').attr('class', 'remove').html("Remover");


        $(btnEdit).on('click', (event) =>{
            Posts.enableEdit(event.target);
        });

        $(btnSave).on('click', (event) =>{
            Posts.update(event.target);
        });

        $(btnRemove).on('click', (event) =>{
            Posts.remove(event.target);
        });

        $(user).append(date);
        
        $(comment).append(content); 
        $(comment).append(user);

        $(comment).append(btnEdit);
        $(comment).append(btnSave);
        $(comment).append(btnRemove);
        

        
        $("#comments").append(comment);




        // var comment = document.createElement("div"); 
        
        // comment.setAttribute("id", "comment-" + data.id); 
        // comment.setAttribute("class", "comment");

        
        
        // var content = document.createElement("p"); 
        // content.setAttribute("class", "content");
        // content.innerHTML = data.content;
        
        // var user= document.createElement("p"); 
        // user.innerHTML = "Por:" + data.user.firstname + " " + data.user.lastname + " ";
        // user.setAttribute("class", "user");


        // var date = document.createElement("span"); 
        // date.setAttribute("class", "date");
        

        // var dtCreation = new Date(data.createdAt); 

        // date.innerHTML = (dtCreation.getDate() < 10 ? "0" + dtCreation.getDate(): dtCreation.getDate()) + 
        // "/"+ ((dtCreation.getMonth() + 1) < 10 ? "0" + (dtCreation.getMonth()+ 1):(dtCreation.getMonth()+1))+
        // "/"+ dtCreation.getFullYear();

        // user.append(date);
        // comment.append(content);
        // comment.append(user);

        // var comments = document.getElementById("comments"); 
        // comments.append(comment);

    },

    findAll : () => {

        $.ajax({
            type: "GET",
            url: '/post',
            data : {'content':$('#content-search').val()},
            success: (data) => { 
                $('#comments').empty();
                for(var post of data)   { 
                    Posts.template(post);
                }
            },
            
            error: () => {
                console.log("Ocorreu um erro");
            },
            dataType: 'json'
        });
    },

    enableEdit : (button) => {
        var comment = $(button).parent();

        $(comment).children('textarea').prop('disabled', false); 
        $(comment).children('button.edit').hide();
        $(comment).children('button.save').show();

    },


    update : (button) => {
        var comment = $(button).parent();

        var id = $(comment).attr('id').replace('comment-', '');
        var content = $(comment).children('textarea').val();

        $.ajax({
            type: "PUT",
            url: '/post',
            data : {'content':content, 'id':id},
            success: (data) => { 
                $(comment).children('textarea').prop('disabled', true); 
                $(comment).children('button.edit').show();
                $(comment).children('button.save').hide();
            },
            
            error: () => {
                console.log("Ocorreu um erro");
            },
            dataType: 'json'
        });

        return false;


    },

    remove : (button) => {
        var comment = $(button).parent();

        var id = $(comment).attr('id').replace('comment-', '');

        $.ajax({
            type: "DELETE",
            url: '/post',
            data : {'id':id},
            success: (data) => { 
                $(comment).remove();
            },
            
            error: () => {
                console.log("Ocorreu um erro");
            },
            dataType: 'json'
        });

        return false;


    }

}


User = {
    findAll : () => {

        $.ajax({
            type: "GET",
            url: '/usuarios',
            success: User.loadAll
        }); 
    },

    loadAll: (data) => {
        var userCombo = $('#users');

        for(user of data){
            userCombo.append($('<option></option>').attr('value', user.id).html(user.firstname + ' ' + user.lastname));
        }
    },

    create : () => {

        var t = {};

        t.firstname = $("#firstname").val();
        t.lastname = $("#lastname").val();
        
        $.ajax({
            type: 'POST',
            url: '/usuarios',
            data : t, 
            dataType: "json",
            success: (data) => { 
                $('#users').empty();
                User.findAll();
            },
        });
        return false;
    },    
}

$(document).ready(
    () => {
        User.findAll();
        Posts.findAll();
    }
)