try {
  
  
  const express = require('express');
  const app = express();
  const port = 3000;
  //const path = require('path');
  const bodyParser = require('body-parser');
  const db = require('./src/configs/sequelize');

  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({extended:true}));

  
  app.use(express.static("public"));

  // db.sequelize.sync({force:true}).then(()=>{
  //     console.log('teste');
  // }).catch((err) => {
  //     console.log('teste2');
  // });


  db.sequelize.sync().then(()=>{
    console.log('teste');
}).catch((err) => {
    console.log('teste2');
});


  require('./src/user/routes')(app);
  require('./src/posts/routes')(app);

  app.get("/", (req,res) => {
    res.sendFile(__dirname + "/public/views/index.html");
  });


  app.listen(port, () => {
      console.log(`Example app listening at http://localhost:${port}`);
  })

} catch (error) {
  console.log('error;');
}